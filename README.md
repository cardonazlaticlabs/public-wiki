# Public Wiki

**All content is on the [public-facing wiki here](https://gitlab.com/cardonazlaticlabs/public-wiki/-/wikis/home)**

A publicly-accessible complement to the Cardona and Zlatic labs' [internal wiki](https://gitlab.com/cardonazlaticlabs/meta/-/wikis/home).
Information must be safe to publish to the world; useful for onboarding external collaborators. 

